package org.tafat;

import monopoly.*;
import org.junit.Before;
import tafat.Simulation;
import tafat.TafatPlatform;
import tara.magritte.Instance;
import tara.magritte.Model;
import tara.magritte.NativeCode;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class Test {

	TafatPlatform platform;
	MonopolyApplication domain;

	@Before
	public void setUp() throws Exception {
		Model model = Model.load().init(MonopolyApplication.class, TafatPlatform.class);
		model.<TafatPlatform>platform().userInterface().remove();
		model.platform().init();
		platform = model.platform();
		domain = model.application();
	}

	@org.junit.Test
	public void checkTypes() throws Exception {
		Simulation simulation = platform.simulation();
		assertEquals(1, simulation._instance().types().size());
		assertTrue(simulation._instance().is("Simulation"));

		Dices dices = domain.dices();
		assertThat(dices._instance().types().size(), is(2));
		assertTrue(dices._instance().is("Entity"));
		assertTrue(dices._instance().is("Dices"));

		Board board = domain.board();
		assertThat(board._instance().types().size(), is(2));
		assertTrue(board._instance().is("Entity"));
		assertTrue(board._instance().is("Board"));
		assertThat(board.square(0)._instance().types().size(), is(3));
		assertTrue(board.square(0)._instance().is("Entity"));
		assertTrue(board.square(0)._instance().is("Square"));
		assertTrue(board.square(0)._instance().is("Init"));

		Card card = null; // domain.luckyCards().card(0);
		assertThat(card._instance().types().size(), is(2));
		assertTrue(card._instance().is("Entity"));
		assertTrue(card._instance().is("Card"));

		Player player = domain.playerList().get(0);
		assertThat(player._instance().types().size(), is(5));
		assertTrue(player._instance().is("Player"));
		assertTrue(player._instance().is("Entity"));
		assertTrue(player._instance().is("MoverPlayer"));
		assertTrue(player._instance().is("BehaviorEntity"));
		assertTrue(player._instance().is("Behavior"));
	}

	@org.junit.Test
	public void checkVariables() throws Exception {
		Simulation simulation = platform.simulation();
		assertThat(simulation._instance().variables().size(), is(2));
		assertThat(simulation._instance().variables().get("from").get(0), is(asDate("01/01/2015 00:00:00")));
		assertThat(simulation._instance().variables().get("to").get(0), is(asDate("02/01/2015 00:00:00")));

		Dices dices = domain.dices();
		assertThat(dices._instance().variables().size(), is(5));
		assertThat(dices._instance().variables().get("value1").get(0), is(0));
		assertThat(dices._instance().variables().get("value2").get(0), is(0));
		assertTrue(dices._instance().variables().get("roll").get(0) instanceof NativeCode);
		assertTrue(dices._instance().variables().get("roll").get(0).getClass().getSimpleName().contains("Roll"));
		assertTrue(dices._instance().variables().get("doubles").get(0) instanceof NativeCode);
		assertTrue(dices._instance().variables().get("doubles").get(0).getClass().getSimpleName().contains("Doubles"));
		assertTrue(dices._instance().variables().get("doubles").get(0) instanceof NativeCode);
		assertTrue(dices._instance().variables().get("doubles").get(0).getClass().getSimpleName().contains("Doubles"));

		Board board = domain.board();
		assertThat(board._instance().variables().size(), is(3));
		assertTrue(board._instance().variables().get("squareAt").get(0) instanceof NativeCode);
		assertTrue(board._instance().variables().get("squareAt").get(0).getClass().getSimpleName().contains("SquareAt"));
		assertTrue(board._instance().variables().get("squareOf").get(0) instanceof NativeCode);
		assertTrue(board._instance().variables().get("squareOf").get(0).getClass().getSimpleName().contains("SquareOf"));
		assertTrue(board._instance().variables().get("positionOf").get(0) instanceof NativeCode);
		assertTrue(board._instance().variables().get("positionOf").get(0).getClass().getSimpleName().contains("Position"));

		Card card = null; // domain.luckyCards().card(0);
		assertThat(card._instance().variables().size(), is(2));
		assertThat(card._instance().variables().get("moveTo").get(0), is(-1000));
		assertTrue(card._instance().variables().get("transport").get(0) instanceof NativeCode);
		assertTrue(card._instance().variables().get("transport").get(0).getClass().getSimpleName().contains("Transport"));

		Player player = domain.playerList().get(0);
		assertThat(player._instance().variables().size(), is(7));
		assertThat(player._instance().variables().get("id").get(0), is("p1"));
		assertThat(((Square)player._instance().variables().get("square").get(0))._name(), is("Model#board$s0"));
		assertThat(player._instance().variables().get("step").get(0), is(1));
		assertThat(player._instance().variables().get("timeout").get(0), is(0));
		assertThat(player._instance().variables().get("turnsToBeInJail").get(0), is(0));
		assertNotNull(player._instance().variables().get("checkStep").get(0));
		assertThat(player._instance().variables().get("numberOfRolls").get(0), is(0));
	}

	@org.junit.Test
	public void checkComponents() throws Exception {
		Simulation simulation = platform.simulation();
		assertThat(simulation._instance().components().size(), is(0));

		Dices dices = domain.dices();
		assertThat(dices._instance().components().size(), is(0));

		Board board = domain.board();
		assertThat(board._instance().components().size(), is(40));
		for (Instance instance : board._instance().components()) assertTrue(instance.is("Square"));

		Card card = null; // domain.luckyCards().card(0);
		assertThat(card._instance().components().size(), is(0));

		Player player = domain.playerList().get(0);
		assertThat(player._instance().components().size(), is(8));
		assertTrue(player._instance().components().get(0).is("Behavior$Start"));
		assertTrue(player._instance().components().get(1).is("MoverPlayer$Rule"));
		assertTrue(player._instance().components().get(1).is("MoverPlayer$PlayerIsJailed"));
		assertTrue(player._instance().components().get(2).is("MoverPlayer$Rule"));
		assertTrue(player._instance().components().get(2).is("MoverPlayer$JailAfterThreeDoubles"));
		assertTrue(player._instance().components().get(3).is("MoverPlayer$Rule"));
		assertTrue(player._instance().components().get(3).is("MoverPlayer$Advance"));
		assertTrue(player._instance().components().get(4).is("MoverPlayer$Rule"));
		assertTrue(player._instance().components().get(4).is("MoverPlayer$ToJailWhenInGoToJailSquare"));
		assertTrue(player._instance().components().get(5).is("MoverPlayer$Rule"));
		assertTrue(player._instance().components().get(5).is("MoverPlayer$CheckCard"));
		assertTrue(player._instance().components().get(6).is("MoverPlayer$Rule"));
		assertTrue(player._instance().components().get(6).is("MoverPlayer$Doubles"));
		assertTrue(player._instance().components().get(7).is("Action"));
		for (int i = 1; i < 7; i++) assertTrue(player._instance().components().get(i).is("Behavior$Knol"));
	}

	private LocalDateTime asDate(String date) {
		return LocalDateTime.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
	}
}
