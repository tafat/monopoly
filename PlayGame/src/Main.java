import tara.magritte.Model;

public class Main {

	public static void main(String[] args) {
		Model model = Model.load().init(monopoly.MonopolyApplication.class, tafat.TafatPlatform.class);
		model.platform().init();
		model.platform().execute();
	}
}