package natives.simulation.userinterface.heatmap.square;

import monopoly.MonopolyApplication;
import tara.magritte.Model;

public class Value {

	public static double valueOf(Model model, String id) {
		return model.<MonopolyApplication>application().board().squareList().stream()
				.filter(s -> s._simpleName().equals(id))
				.findFirst().get().count();
	}
}