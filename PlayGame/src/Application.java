import tara.magritte.Model;

import static java.lang.System.nanoTime;

public class Application {

	public static void main(String[] args) {
		long init = nanoTime();
		Model model = Model.load().init(monopoly.MonopolyApplication.class, tafat.TafatPlatform.class);
		model.platform().init();
		model.platform().execute();
		System.out.println(((nanoTime() - init) / 1e9));
	}
}