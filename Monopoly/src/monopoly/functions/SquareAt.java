package monopoly.functions;

import monopoly.Square;

public interface SquareAt {

    Square squareAt(int position);

}
