package monopoly.functions;

import monopoly.Square;

public interface SquareOf {

    Square squareOf(String type);

}
