package monopoly.functions;

import monopoly.Card;

public interface Get {

    Card get();

}
