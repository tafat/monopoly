package monopoly.functions;

public interface Movement {

    boolean involvesMovement();

}
