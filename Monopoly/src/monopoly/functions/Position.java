package monopoly.functions;

import monopoly.Square;

public interface Position {

    int position(Square square);

}
